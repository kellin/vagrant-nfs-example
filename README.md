This is an example of how to use NFS directories in Vagrant on Fedora.

# Prerequisites:

The following all expects you to be on Fedora and are tested on Fedora 26.

## Packages

- virt-manager
- libvirt
- libvirt-client
- vagrant
- vagrant-libvirt
- nfs-utils
- nfs-server

## Services

- nfs-server

## Firewall on Host

- open service ports for nfs, rpc-bind, mountd

## Easy Copy Paste Setup

    dnf install virt-manager libvirt libvirt-client vagrant vagrant-libvirt nfs-utils nfs-server -y
    systemctl restart nfs-server
    firewall-cmd --permanent --add-service=nfs
    firewall-cmd --permanent --add-service=rpc-bind
    firewall-cmd --permanent --add-service=mountd
    firewall-cmd --reload

# General Issues

This setup will add an export to `/etc/export` as expected and will show up
in the guest machine using `showmount -e HOST_IP`. The problem comes when
the vagrant guest actually tries to mount the NFS share. Access to the
share will be denied due to permissions.

To test permissions - you can construct a filepath with the same
permissions in the tree and the directory and add that to `/etc/exports`.
This will show up in `showmount -e HOST_IP` in the guest AND be mountable
unlike the default Vagrant directory.


## Example /etc/exports

    /home/USERNAME/test 192.168.121.X(rw,nosubtree_check,all_squash,anonuid=1000,anongid=1000)
    # VAGRANT-BEGIN:
    # This will be populated by vagrant
    # VAGARNT-END
